# jackal-postgres-etcd-values

Repo storing Helm config values for postgres database, and etcd storage that should be used in combination with Jackal server. 

*Commands from root of the folder jackal-postgres-etcd-monitoring-values. Don't forget to set right kubectl context.*

## Installation

PostgreSQL + etcd
```
kubectl -n challenge \
        apply \
        -f postgres-init-db-configmap.yaml
 helm upgrade --install \
              --namespace challenge \
              postgres \
              -f values-postgres.yaml \
              bitnami/postgresql
 helm upgrade --install \
              --namespace challenge \
              --set auth.rbac.rootPassword=root \
              etcd -f values-etcd.yaml \
              bitnami/etcd
```

Monitoring
```
 kubectl  create ns monitoring  
 helm -n monitoring \
      upgrade --install kube-prometheus-stack \
      -f values-kube-prometheus.yaml \
      prometheus-community/kube-prometheus-stack
```